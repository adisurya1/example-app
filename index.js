const PORT = 10500;

const express = require('express');

const app = express();

app.get('/', (req, res) => {
	res.send("example app is running");
});

app.listen(PORT, () => {
	console.log(`example app listening port ${PORT}`);
});
