# base image
FROM node:16

# expose application port
EXPOSE 10500

# create app directory in image
WORKDIR /usr/src/example-app

COPY package*.json ./

RUN npm ci

# bundle app source in image
COPY . .

CMD ["node", "index.js"]
